#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo
echo "#############################################"
echo "Configuring locales and keymaps..."
echo "#############################################"
echo

ln -sf /usr/share/zoneinfo/Europe/Ljubljana /etc/localtime
hwclock --systohc

echo "LANG=en_GB.UTF-8" > /etc/locale.conf
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_GB ISO-8859-1" >> /etc/locale.gen
locale-gen

echo "KEYMAP=uk" >> /etc/vconsole.conf

echo
echo "#############################################"
echo "Installing the bootloader..."
echo "#############################################"
echo

bootctl --path=/boot install

cat << 'EOF' > /boot/loader/loader.conf
default arch
timeout 3
console-mode max
editor no
EOF

cat << 'EOF' > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
EOF

echo "options root=UUID=$(lsblk -n -o UUID /dev/nvme0n1p2) rw" >> /boot/loader/entries/arch.conf

echo
echo "#############################################"
echo "Adding a user..."
echo "#############################################"
echo

echo -n "Password for root: "
read < /dev/tty -s rootpassword 
echo
echo -n "Repeat Password for root: "
read < /dev/tty -s rootpassword2
echo
[[ "$rootpassword" == "$rootpassword2" ]] || ( echo "Passwords did not match"; exit 1; )

echo -n "Username: "
read -r < /dev/tty username
: "${username:?"Missing username"}"

echo -n "Password for $username: "
read -r < /dev/tty -s userpassword
echo
echo -n "Repeat Password for $username: "
read -r < /dev/tty -s userpassword2
echo
[[ "$userpassword" == "$userpassword2" ]] || ( echo "Passwords did not match"; exit 1; )

useradd -m -g wheel -s /usr/bin/zsh "$username"
echo "$username:$userpassword" | chpasswd
echo "root:$rootpassword" | chpasswd
unset userpassword userpassword2 rootpassword rootpassword2

echo "%wheel ALL=(ALL) ALL" >> etc/sudoers
echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

echo -n "Hostname: "
read -r < /dev/tty hostname
: "${hostname:?"Missing hostname"}"
echo "${hostname}" > /etc/hostname
rm /etc/hosts
echo "127.0.0.1	localhost" > /etc/hosts
echo "::1		localhost" >> /etc/hosts
echo "127.0.1.1	${hostname}.localdomain	${hostname}" >> /etc/hosts

echo
echo "#############################################"
echo "Base system install finished."
echo "Installing additional packages..."
echo "#############################################"
echo

grep "^Color" /etc/pacman.conf >/dev/null || sed -i "s/^#Color/Color/" /etc/pacman.conf

pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1
pacman --noconfirm --needed -S xf86-video-ati mesa alsa-utils pulseaudio-alsa pavucontrol firefox nm-connection-editor xdg-user-dirs \
network-manager-applet xorg-server xorg-xinit xfce4 efibootmgr zsh keepassxc man-db redshift xfce4-whiskermenu-plugin viewnior gnome-disk-utility \
noto-fonts-cjk ttf-cascadia-code  ttf-liberation ttf-dejavu thunar-volman xfce4-notifyd xfce4-pulseaudio-plugin

systemctl enable fstrim.timer NetworkManager.service

echo
echo "#############################################"
echo "Downloading dotfiles &"
echo "installing the yay AUR helper..."
echo "#############################################"
echo

cd /tmp
wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz https://gitlab.com/xnay/dotfiles/-/archive/master/dotfiles-master.tar.gz
sudo -u "$username" tar -xzf dotfiles-master.tar.gz &&
sudo -u "$username" rsync -a dotfiles-master/ /home/"$username"

sudo -u "$username" tar -xzf yay.tar.gz && cd yay &&
sudo -u "$username" makepkg --noconfirm -si
